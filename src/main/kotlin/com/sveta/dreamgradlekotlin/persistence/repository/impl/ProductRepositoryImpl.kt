package com.sveta.dreamgradlekotlin.persistence.repository.impl

import com.sveta.dreamgradlekotlin.persistence.repository.IProductRepository
import org.jooq.DSLContext
import org.jooq.example.db.h2.public_.Tables.PRODUCT
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.jooq.impl.DSL
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Repository
class ProductRepositoryImpl : IProductRepository {
    @Autowired
    lateinit var dsl: DSLContext


    override fun findById(id: Int): Product? {
        val products = dsl.select().from(PRODUCT).where(PRODUCT.ID.equal(id)).fetch()
                .into(Product::class.java)
        val product = if (products.isEmpty()) null else products[0]
        return product
    }

    override fun findAll(): List<Product>? {
        return dsl.select().from(PRODUCT).fetch().into(Product::class.java)
    }

    override fun createProduct(product: Product): Int? {
        return dsl.insertInto(PRODUCT, PRODUCT.NAME, PRODUCT.PRICE, PRODUCT.DESCRIPTION, PRODUCT.REALIZATION_DATE)
                .values(product.name, product.price, product.description, product.realizationDate)
                .returning()
                .fetchOne()
                ?.into(Product::class.java)
                ?.id
    }

    override fun deleteProduct(id: Int) {
        dsl.deleteFrom(PRODUCT).where(PRODUCT.ID.equal(id)).execute();
    }

    override fun updateProduct(product: Product): Product? {
        dsl.update(PRODUCT)
                .set(DSL.row(PRODUCT.NAME, PRODUCT.PRICE, PRODUCT.DESCRIPTION, PRODUCT.REALIZATION_DATE),
                        DSL.row(product.name, product.price, product.description, product.realizationDate))
                .where(PRODUCT.ID.eq(product.id))
                .execute();
        return findById(product.id)
    }

    @PostConstruct
    fun created() {
        LOG.info("POST CONSTRUCT in ProjectServiceImpl")
    }

    @PreDestroy
    fun onDestroy() {
        LOG.info("PRE DESTROY in ProjectServiceImpl")
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger("ProjectRepositoryImpl")
    }
}
