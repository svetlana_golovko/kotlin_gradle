package com.sveta.dreamgradlekotlin.spring

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import javax.annotation.PostConstruct

@Configuration
class PersistenceConfig {
    @Autowired
    private val environment: Environment? = null
    @PostConstruct
    private fun postConstruct() {
        LOG.info("project suffix: {}", environment!!.getProperty("project.suffix"))
    }

    companion object {
        private val LOG: Logger = LoggerFactory.getLogger("PersistenceConfig")
    }
}
