package com.sveta.dreamgradlekotlin.controller

import com.sveta.dreamgradlekotlin.service.IProductService
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(value = ["/products"])
class ProductController {
    @Autowired
    lateinit var productServiceImpl: IProductService

    @GetMapping(path = ["/{id}"])
    fun findOne(@PathVariable id: Int): Product? {
        return productServiceImpl.findById(Integer.valueOf(id))
    }

    @GetMapping(path = ["/"])
    fun findAll(): List<Product>? {
        return productServiceImpl.findAll()
    }

    @PostMapping
    fun create(@RequestBody product: Product): Int? {
        return productServiceImpl.createProduct(product)
    }

    @DeleteMapping(path = ["/{id}"])
    fun delete(@PathVariable id: Int) {
        productServiceImpl.deleteProduct(id)
    }

    @PutMapping
    fun update(@RequestBody product: Product): Product? {
        return productServiceImpl.updateProduct(product)
    }

}