package com.sveta.dreamgradlekotlin

import com.sveta.dreamgradlekotlin.persistence.repository.IProductRepository
import org.jooq.example.db.h2.public_.tables.pojos.Product
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class ProductRepositoryIntegrationTest(@Autowired val productRepository: IProductRepository ) {
    @Test
    fun givenNewProduct_thenSavedSuccess() {
        val newProduct = Product(null, "First Project", 3.0, "Description", java.sql.Date.valueOf("2021-01-10"))
        assertNotNull(productRepository.createProduct(newProduct))
    }
}