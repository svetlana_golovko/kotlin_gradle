import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Database
import org.jooq.meta.jaxb.Generate
import org.jooq.meta.jaxb.Generator
import org.jooq.meta.jaxb.Jdbc

buildscript {
	val jooqVersion  = "3.12.3"
	repositories {
		mavenCentral()
	}

	dependencies {
		classpath( group = "org.jooq", name = "jooq", version = jooqVersion)
		classpath( group = "org.jooq", name = "jooq-meta", version = jooqVersion)
		classpath( group = "org.jooq", name = "jooq-codegen", version = jooqVersion)
		classpath( group = "com.h2database", name = "h2", version = "1.4.200")
		classpath("org.jetbrains.kotlin:kotlin-reflect")
	}
}


plugins {
	id("org.springframework.boot") version "2.6.0"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.0"
	kotlin("plugin.spring") version "1.6.0"
}

group = "com.sveta"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	runtimeOnly("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-jooq")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	runtimeOnly("com.h2database:h2")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

sourceSets.main {
	java.srcDirs("src/main/kotlin", "src/main/java/jooq-h2")
}

GenerationTool.generate(Configuration()
		.withJdbc( Jdbc()
				.withDriver("org.h2.Driver")
				.withUrl("jdbc:h2:~/jooq-spring-boot-example;INIT=RUNSCRIPT FROM 'src/main/resources/schema.sql'")
				.withUser("sa")
				.withPassword(""))
		.withGenerator( Generator()
				.withDatabase( Database())
				.withGenerate( Generate()
						.withPojos(true)
						.withImmutablePojos(true)
						.withRecords(false)
						.withDaos(false))
				.withTarget( org.jooq.meta.jaxb.Target()
						.withPackageName("org.jooq.example.db.h2")
						.withDirectory("src/main/java/jooq-h2"))))